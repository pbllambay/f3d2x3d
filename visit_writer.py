"""

Author: Pablo Benitez-Llambay
Email: pbllambay@gmail.com
Date: 6/6/2017

Purpose: writing it in a .VTK file (legacy format).

"""

class VTKFile():
    def __init__(self, xc, yc, zc, name="output.vtk"):
        if name[-4:] != ".vtk":
            name += ".vtk"
        self.name = name
        self.ofile = open(self.name,"w")
        print("VTK file opened")
        self.xc  = xc
        self.yc  = yc
        self.zc  = zc
        self.nxc = self.xc.shape[0]
        self.nyc = self.yc.shape[0]
        self.nzc = self.zc.shape[0]
        self.write_vtk()
        
    def write_vtk(self):
       self.ofile.write("# vtk DataFile Version 2.0\n");
       self.ofile.write("Here you can put a header\n");
       self.ofile.write("BINARY\n");
       self.ofile.write("DATASET RECTILINEAR_GRID\n");
       self.ofile.write("DIMENSIONS {0:d} {1:d} {2:d}\n".format(self.nxc, self.nyc, self.nzc))
       self.ofile.write("X_COORDINATES {0:d} FLOAT\n".format(self.nxc))
       (self.xc.astype('float32').byteswap()).tofile(self.ofile)
       self.ofile.write("\n")
       self.ofile.write("Y_COORDINATES {0:d} FLOAT\n".format(self.nyc))
       (self.yc.astype('float32').byteswap()).tofile(self.ofile)
       self.ofile.write("\n")
       self.ofile.write("Z_COORDINATES {0:d} FLOAT\n".format(self.nzc))
       (self.zc.astype('float32').byteswap()).tofile(self.ofile)
       self.ofile.write("\n")
       self.ofile.write("POINT_DATA {0:d}\n".format(self.nxc*self.nyc*self.nzc))
       
    def add_scalar(self,name,field):
          print("Writing {:s}".format(name))
          self.ofile.write("SCALARS {:s} FLOAT\n".format(name))
          self.ofile.write("LOOKUP_TABLE default\n");
          (field.astype('float32').byteswap()).tofile(self.ofile)
          print("{:s} ready".format(name))

    def close(self):
        self.ofile.close()
        print("VTK file closed")
