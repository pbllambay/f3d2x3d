"""

Author: Pablo Benitez-Llambay
Date: 6/6/2017

Purpose: Generating cubic data from FARGO3D and
         writing it in a .VTK file (legacy format)

"""

import numpy as np
import visit_writer
import trilinear as t

def extend_field(field):
    extended_field = np.ndarray([field.shape[0],field.shape[1],field.shape[2]+1])
    extended_field[:,:,:-1] = field
    extended_field[:,:,-1]  = field[:,:,0]
    return extended_field

### PARAMETERS--------------------------------------------------------
#Directory where the data is stored (e.g outputs/p3diso)
directory = "fargo3d_directory/outputs/p3diso/" 

#This is the output number of your data
n = 10 
###-------------------------------------------------------------------

#Note: Data has to be inside the interval [-pi,pi]
phi   = np.loadtxt(directory+"domain_x.dat")
r     = np.loadtxt(directory+"domain_y.dat")[3:-3]
theta = np.loadtxt(directory+"domain_z.dat")[3:-3]

rmed        = 0.5*(r[1:]+r[:-1])
thetamed    = 0.5*(theta[1:]+theta[:-1])
phimed      = np.ndarray(phi.shape)
phimed[:-1] = 0.5*(phi[1:]+phi[:-1])
phimed[-1]  = phimed[-2] + (phi[1]-phi[0])

#Cartesian mesh, change these numbers according to your needs
nxc = 600
nyc = 600
nzc = 40

xc = np.linspace(-r.max(),r.max(),nxc)
yc = np.linspace(-r.max(),r.max(),nyc)
zc = np.linspace(r.max()*np.cos(thetamed.max()),r.max()*np.cos(thetamed.min()),nzc)

YC,ZC,XC = np.meshgrid(yc,zc,xc)

#I remove one element from r, theta
r = r[:-1]
theta = theta[:-1]

#---------------------------------------------------------------------

OutputFile = visit_writer.VTKFile(xc,yc,zc)
    
field_ext = extend_field(np.log10(np.fromfile(directory+f"gasdens{n}.dat").reshape(theta.shape[0],r.shape[0],phi.shape[0]-1)))
density = t.trilinear(field_ext,rmed,phimed,thetamed,xc,yc,zc,field_ext.min())
OutputFile.add_scalar("Density",density)

field  = np.fromfile(directory+f"gasvx{n}.dat").reshape(theta.shape[0],r.shape[0],phi.shape[0]-1)
field_ext = extend_field(field)
vphi = t.trilinear(field_ext,rmed,phi,thetamed,xc,yc,zc,0.0)
    
field_ext = extend_field(np.fromfile(directory+f"gasvy{n}.dat").reshape(theta.shape[0],r.shape[0],phi.shape[0]-1))
vrad = t.trilinear(field_ext,r,phimed,thetamed,xc,yc,zc,0.0)

field_ext = extend_field(np.fromfile(directory+f"gasvz{n}.dat").reshape(theta.shape[0],r.shape[0],phi.shape[0]-1))
vtheta = t.trilinear(field_ext,rmed,phimed,theta,xc,yc,zc,0.0)

#Creating the cartesian velocity

thetac = np.arctan2(np.sqrt(XC**2+YC**2),ZC)
rc     = np.sqrt(XC**2+YC**2+ZC**2);
phic   = np.arctan2(YC,XC);	

VX = - vphi*np.sin(phic)   + vrad*np.cos(phic)*np.sin(thetac) + vtheta*np.cos(phic)*np.cos(thetac);
VY =   vphi*np.cos(phic)   + vrad*np.sin(phic)*np.sin(thetac) + vtheta*np.sin(phic)*np.cos(thetac);
VZ =   vrad*np.cos(thetac) - vtheta*np.sin(thetac);

OutputFile.add_scalar("vx",VX)
OutputFile.add_scalar("vy",VY)
OutputFile.add_scalar("vz",VZ)

OutputFile.close()
