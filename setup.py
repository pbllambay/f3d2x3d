from distutils.core import setup, Extension
import numpy as np

module = Extension('trilinear',
                   sources = ['trilinear.c'])

setup (name         = 'PackageName',
       version      = '0.1',
       description  = 'Trilinear interpolation from a spherical mesh',
       author       = 'Pablo Benitez-Llambay',
       author_email = 'pbllambay@gmail.com',
       requires     = ['numpy',],
       ext_modules  = [module],
       include_dirs = [np.get_include(),])
